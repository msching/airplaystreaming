//
//  JKAudioDevice.m
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/14/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import "JKAudioDevice.h"

#import "JKAudioSource.h"

@implementation JKAudioDevice {
	NSString *_name;
	AudioObjectPropertyListenerBlock _sourceListenerBlock;
}

@synthesize identifier=_identifier;

- (id) initWithDeviceID:(AudioDeviceID)deviceID {
	self = [super init];
	
	if (self) {
		_deviceID = deviceID;
		
		UInt32 propsize;
		
		AudioObjectPropertyAddress addr;
		addr.mSelector = kAudioDevicePropertyStreams;
		addr.mScope = kAudioDevicePropertyScopeInput;
		addr.mElement = kAudioObjectPropertyElementWildcard;
		
		AudioObjectGetPropertyDataSize(_deviceID, &addr, 0, NULL, &propsize);
		int numberOfInputStreams = propsize / sizeof(AudioStreamID);
		
		_isInput = numberOfInputStreams > 0;
		
		_name = [self getStringProperty:kAudioObjectPropertyName forChannel:kAudioObjectPropertyScopeGlobal];
		
		unsigned int transportType = 0;
		[self getProperty:kAudioDevicePropertyTransportType forChannel:kAudioObjectPropertyScopeGlobal datasize:sizeof(transportType) ptr:&transportType];
		
		_isAirplay = (transportType == kAudioDeviceTransportTypeAirPlay);
		_isBuiltIn = (transportType == kAudioDeviceTransportTypeBuiltIn);
		
		__weak id weakSelf = self;
		_sourceListenerBlock = ^(UInt32 inNumberAddresses, const AudioObjectPropertyAddress *inAddresses) {
			[weakSelf willChangeValueForKey:@"audioSources"];
			[weakSelf didChangeValueForKey:@"audioSources"];
		};
		
		AudioObjectPropertyAddress sourceAddr;
		sourceAddr.mSelector = kAudioDevicePropertyDataSources;
		sourceAddr.mScope = kAudioDevicePropertyScopeOutput;
		sourceAddr.mElement = kAudioObjectPropertyElementMaster;
		
		AudioObjectAddPropertyListenerBlock(deviceID, &sourceAddr, dispatch_get_current_queue(), _sourceListenerBlock);
	}
	
	return self;
}

- (void) dealloc
{
	AudioObjectPropertyAddress sourceAddr;
	sourceAddr.mSelector = kAudioDevicePropertyDataSources;
	sourceAddr.mScope = kAudioDevicePropertyScopeOutput;
	sourceAddr.mElement = kAudioObjectPropertyElementMaster;
	
	AudioObjectRemovePropertyListenerBlock(self.deviceID, &sourceAddr, dispatch_get_current_queue(), _sourceListenerBlock);
}

- (NSString *) getStringProperty:(unsigned int)selector forChannel:(unsigned int)scope {
	CFStringRef value = NULL;
	UInt32 size = sizeof(value);
	
	/*AudioObjectPropertyAddress addr;
	addr.mSelector = selector;
	addr.mScope = scope;
	addr.mElement = kAudioObjectPropertyElementMaster;
	
	AudioObjectGetPropertyData(_deviceID, &addr, 0, NULL, &size, &value);*/
	
	[self getProperty:selector forChannel:scope datasize:size ptr:&value];
	
	return (__bridge NSString *) value;
}

- (OSStatus) getProperty:(unsigned int)selector forChannel:(unsigned int)scope datasize:(unsigned int)size ptr:(void *)pointer {
	AudioObjectPropertyAddress addr;
	addr.mSelector = selector;
	addr.mScope = scope;
	addr.mElement = kAudioObjectPropertyElementMaster;
	
	return AudioObjectGetPropertyData(_deviceID, &addr, 0, NULL, &size, pointer);
}

- (void) copyProperty:(unsigned int)selector forChannel:(unsigned int)scope datasize:(unsigned int *)size ptr:(void **)pointer {
	
	AudioObjectPropertyAddress addr;
	addr.mSelector = selector;
	addr.mScope = scope;
	addr.mElement = kAudioObjectPropertyElementMaster;
	
	AudioObjectGetPropertyDataSize(_deviceID, &addr, 0, NULL, size);
	
	*pointer = malloc(*size);
	
	AudioObjectGetPropertyData(_deviceID, &addr, 0, NULL, size, *pointer);
}

- (NSString *) identifier {
	if (!_identifier) {
		_identifier = [self getStringProperty:kAudioDevicePropertyDeviceUID forChannel:kAudioObjectPropertyScopeOutput];
	}
	
	return _identifier;
}

- (NSString *) name {
	return _name;
}

- (NSArray *) audioSources {
	NSMutableArray *sources = [NSMutableArray array];
	
	UInt32 *sourceIds = NULL;
	UInt32 dataSize = 0;
	
	[self copyProperty:kAudioDevicePropertyDataSources forChannel:kAudioDevicePropertyScopeOutput datasize:&dataSize ptr:(void **)&sourceIds];
	int numberOfSources = dataSize / sizeof(UInt32);
	
	for (int i=0; i<numberOfSources; i++) {
		JKAudioSource *source = [[JKAudioSource alloc] initWithID:sourceIds[i] device:self];		
		[sources addObject:source];
	}
	
	free(sourceIds);
	
	return sources;
}

- (void) setAudioSource:(JKAudioSource *)audioSource {
	UInt32 sourceID = audioSource.sourceID;
	
	AudioObjectPropertyAddress addr;
	addr.mSelector = kAudioDevicePropertyDataSource;
	addr.mScope = kAudioDevicePropertyScopeOutput;
	addr.mElement = kAudioObjectPropertyElementMaster;
	
	AudioObjectSetPropertyData(_deviceID, &addr, 0, NULL, sizeof(UInt32), &sourceID);
}

- (void) setAudioSources:(NSArray *)sources {
	if ([sources count] < 1) {
		return;
	}
	
	UInt32 sourcesSize = (UInt32) (sizeof(UInt32) * [sources count]);
	UInt32 *sourceIds = malloc(sourcesSize);
	
	for (int i=0; i<[sources count]; i++) {
		sourceIds[i] = ((JKAudioSource *) [sources objectAtIndex:i]).sourceID;
	}
	
	AudioObjectPropertyAddress addr;
	addr.mSelector = kAudioDevicePropertyDataSource;
	addr.mScope = kAudioDevicePropertyScopeOutput;
	addr.mElement = kAudioObjectPropertyElementMaster;
	
	AudioObjectSetPropertyData(_deviceID, &addr, 0, NULL, sourcesSize, sourceIds);
	
	free(sourceIds);
}

@end
